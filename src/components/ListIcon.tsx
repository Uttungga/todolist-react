import React from "react";
import { Todo } from "../model";
import "./style.css";
import { MdDelete } from "react-icons/md";
import { GiCheckMark } from "react-icons/gi";
import { IoIosCheckmarkCircle } from "react-icons/io";

type Props = {
  todo: Todo;
  todos: Todo[];
  setTodos: React.Dispatch<React.SetStateAction<Todo[]>>;
};

const ListIcon = ({ todo, todos, setTodos }: Props) => {
  const Mark = (id: number) => {
    setTodos(
      todos.map((todo) =>
        todo.id === id ? { ...todo, isDone: !todo.isDone } : todo
      )
    );
  };

  const Delete = (id: number) => {
    setTodos(todos.filter((todo) => todo.id !== id));
  };

  return (
    <form className="ListIcon">
      {todo.isDone ? (
        <span className="todo-sigleText">
          {todo.todo} <GiCheckMark />
        </span>
      ) : (
        <span className="todo-sigleText">{todo.todo}</span>
      )}

      <div>
        <span
          className="icon"
          onClick={() => {
            Delete(todo.id);
          }}
        >
          <MdDelete />
        </span>
        <span
          className="icon"
          onClick={() => {
            Mark(todo.id);
          }}
        >
          <IoIosCheckmarkCircle />
        </span>
      </div>
    </form>
  );
};

export default ListIcon;
