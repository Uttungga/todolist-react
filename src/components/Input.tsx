import React from "react";
import "./style.css";
import { IoMdAdd } from "react-icons/io";

interface Props {
  todo: string;
  setTodo: React.Dispatch<React.SetStateAction<string>>;
  AddList: (e: React.FormEvent) => void;
}

const Input = ({ todo, setTodo, AddList }: Props) => {
  // const inputRef = useRef<HTMLInputElement>(null);
  return (
    <div>
      <form
        className="input"
        onSubmit={(e) => {
          AddList(e);
        }}
      >
        <input
          // ref={inputRef}
          type="text"
          placeholder="daftar list ..."
          className="input__box"
          value={todo}
          onChange={(e) => setTodo(e.target.value)}
        />
        <button className="input_submit" type="submit">
          <IoMdAdd />
        </button>
      </form>
    </div>
  );
};

export default Input;
