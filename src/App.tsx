import React, { useState } from "react";
import "./App.css";
import Input from "./components/Input";
import { Todo } from "./model";
import TodoList from "./components/TodoList";

const App: React.FC = () => {
  const [todo, setTodo] = useState<string>("");
  const [todos, setTodos] = useState<Todo[]>([]);

  const AddList = (e: React.FormEvent) => {
    e.preventDefault();
    const cek = todos.findIndex((obj) => {
      return obj.todo === todo;
    });
    if (cek !== -1) {
      console.log(cek);
      alert("sudah masuk To Do List !");
      setTodo("");
    } else if (!todo) {
      alert("Di isi dlu !");
    } else {
      setTodos([...todos, { id: Date.now(), todo, isDone: false }]);
      setTodo("");
    }
  };

  // console.log(todos);

  return (
    <div className="App">
      <h1 className="heading">To-Do-List</h1>
      <Input todo={todo} setTodo={setTodo} AddList={AddList} />
      <TodoList todos={todos} setTodos={setTodos} />
    </div>
  );
};

export default App;
